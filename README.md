## Overview
NoChunkLag prevents players from travelling at **excessively high speeds** to **limit lag from chunk-generation**. This is especially useful in post-1.14 servers as the world format is extremely unoptimised and chunk-generation can cause a lot of lag.

## Wiki
[Features](https://gitlab.com/Zenya4/no-chunk-lag/-/wikis/Features)<br>
[Commands](https://gitlab.com/Zenya4/no-chunk-lag/-/wikis/Commands)<br>
[Permissions](https://gitlab.com/Zenya4/no-chunk-lag/-/wikis/Permissions)

## Files
[config.yml](https://gitlab.com/Zenya4/no-chunk-lag/-/tree/master/core/src/main/resources/config.yml)<br>
[messages.yml](https://gitlab.com/Zenya4/no-chunk-lag/-/tree/master/core/src/main/resources/messages.yml)

## More info
[Download](https://www.spigotmc.org/resources/%E2%9D%8C%E2%9C%88%EF%B8%8F-nochunklag-%E2%9C%88%EF%B8%8F%E2%9D%8C-elytra-and-trident-speed-nerf-cooldowns-more.72879/) (SpigotMC)<br>
[Support](https://discord.gg/KGuaxpM) (Discord)

## Credits
This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.<br><br>
Copyright © Zenya4 ([Zenya#0093](https://discord.gg/KGuaxpM) on Discord)